__author__ = 'wubin'

import logging
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from crawler.items.article import Article

logger = logging.getLogger(__name__)

class ArticleSpider(CrawlSpider):
    name = 'doaj'
    allowed_domains = ['doaj.org']

    def __init__(self, lang='English', *args, **kwargs):
        super(ArticleSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['http://doaj.org/search?source={%22query%22:{%22filtered%22:{%22query%22:{%22match_all%22:{}},%22filter%22:{%22bool%22:{%22must%22:[{%22term%22:{%22index.language.exact%22:%22' \
            + lang + '%22}}]}}}},%22size%22:100}']

    def parse_article(self, response):
        for tr in response.xpath('//table[@id="facetview_results"]/tr'):
            article = Article()
            article['title'] = tr.xpath('td[1]//span[@class="title"]/text()').extract()[0].strip()