__author__ = 'wubin'

import logging

logger = logging.getLogger(__name__)

class Fibonacci(object):
    def fib(self):
        a, b = 1, 1
        logger.debug("value a: %d, b: %d", a, b)

        while 1:
            yield a
            a, b = b, (a + b)
            logger.debug("value a: %d, b: %d", a, b)
