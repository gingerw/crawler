__author__ = 'wubin'

import logging
import scrapy

logger = logging.getLogger(__name__)

class Article(scrapy.Item):
    issn = scrapy.Field()
    pissn = scrapy.Field()
    eissn = scrapy.Field()
    doi = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    author = scrapy.Field()
    abstract = scrapy.Field()
    full_text = scrapy.Field()
    subjects = scrapy.Field()
    keywords = scrapy.Field()
    date_published = scrapy.Field()
    publisher = scrapy.Field()
    published_in = scrapy.Field()
    country_of_pub = scrapy.Field()
    journal_lang = scrapy.Field()
    dxdoi = 'dx.doi.org'

    def __str__(self):
       return 'ISSN: '  + self.issn + \
           'PISSN: ' + self.pissn + \
           'EISSN: ' + self.eissn + \
           'DOI: ' + 'http://' + self.dxdoi + '/' + self.doi + \
           'URL: ' + self.url + \
           'Title: ' + self.title + \
           'Author: ' + self.author + \
           'Abstract: ' + self.abstract + \
           'Full Text: ' + self.full_text + \
           'Subjects: ' + self.subjects + \
           'Keywords: ' + self.keywords + \
           'Date published: ' + self.date_published + \
           'Publisher: ' + self.publisher + \
           'Published in: ' + self.published_in + \
           'Country of publish: ' + self.country_of_pub + \
           'Journal Language: ' + self.journal_lang