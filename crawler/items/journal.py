__author__ = 'wubin'

import logging
import scrapy

logger = logging.getLogger(__name__)

class Journal(scrapy.Item):
    name = scrapy.Field()
    description = scrapy.Field()
    url = scrapy.Field()
    field = scrapy.Field()
