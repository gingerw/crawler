__author__ = 'wubin'

import sys
import os

projectdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(projectdir)

from crawler.math.fibonacci import Fibonacci
import logging

logger = logging.getLogger(__name__)

def createdir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)

def main():
    logger.info('Python main started')

    fib = Fibonacci()
    counter = 1
    for i in fib.fib():
        logger.debug('the %dth number is: %d', counter, i)
        if counter == 10:
            break
        counter += 1

    logger.info('Python main ended')

if __name__ == '__main__':
    import logging.config
    # setup logging
    '''
    logging.basicConfig(
        format='%(asctime)s %(levelname)s %(name)s: %(message)s',
        filename='../log/main.log',
        level=logging.DEBUG)
    '''
    logconffile = os.path.join(projectdir, 'conf', 'log.conf')
    logdir = os.path.join(projectdir , 'log')
    createdir(logdir)
    logfile = os.path.join(logdir, 'main.log')
    logging.config.fileConfig(logconffile,
        defaults={'logfile': logfile},
        disable_existing_loggers=False)

    main()

# -- coding: utf-8 --
