__author__ = 'wubin'

from distutils.core import setup

setup(
    name='crawler',
    version='0.0.1',
    packages=['crawler', 'crawler.math',],
    platforms='python-3.4',
    license='Free and Open Source',
    author='Bin Wu',
    author_email='bindiego@outlook.com',
    long_description=open('README.md').read(),
)
