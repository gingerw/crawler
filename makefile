pypy := $(shell which pypy)

.PHONY:
pkg:
	@python3 setup.py sdist

.PHONY:
clean:
	@rm -rf dist MANIFEST log/*

.PHONY:
run:
	@$(pypy) ./crawler/main.py
